from json import dumps
from os import path
from socket import error as socket_error
from socket import socket, AF_INET, SOCK_DGRAM
from time import sleep

import xbmc
from xbmc import Monitor, executebuiltin
from xbmcplugin import endOfDirectory
from xbmcvfs import File

# put everything in single file here to increase startup performance
from resources.lib.communication.socket_shared import BUFFER_SIZE, SOCKET_FILE


class SocketClient:
    def __init__(self):
        self.socket = socket(AF_INET, SOCK_DGRAM)

    def send(self, msg, address):
        self.socket.sendto(msg.encode('utf-8'), address)
        return self.socket.recvfrom(BUFFER_SIZE)

    def settimeout(self, number):
        self.socket.settimeout(number)

    @staticmethod
    def address_exists():
        return path.isfile(SOCKET_FILE)

    @staticmethod
    def get_address():
        f = File(SOCKET_FILE, 'r')
        address = f.read()
        f.close()
        if address:
            address = address.split(':')
            if len(address) > 1:
                return address[0], int(address[1])


def stop_if_updating(handle):
    if not SocketClient.address_exists():
        xbmc.log("STOPPED BECAUSE UPDATING",level=xbmc.LOGINFO)
        endOfDirectory(handle, cacheToDisc=False)
        from resources.lib.gui.info_dialog import InfoDialog
        from resources.lib.const import LANG, ROUTE
        from resources.lib.utils.kodiutils import get_string, replace_plugin_url, focus_list_item
        InfoDialog(get_string(LANG.PLUGIN_UNAVAILABLE_DURING_UPDATE), get_string(LANG.UPDATE_IN_PROGRESS),
                   sound=True).notify()
        # xbmc_monitor = Monitor()
        # xbmc_monitor.waitForAbort(0.1)
        # replace_plugin_url("addons://repository.hacky/xbmc.addon.video")
        # xbmc_monitor.waitForAbort(0.5)
        # focus_list_item(1)
        # xbmc.log("WAITING FOR UPDATE",level=xbmc.LOGINFO)
        # from resources.lib.utils.kodiutils import busy_dialog
        # with busy_dialog():
        #     while True:
        #         xbmc.log("WAITING FOR SERVICE START",level=xbmc.LOGINFO)
        #         address = socket_client.get_address()
        #         if address or xbmc_monitor.abortRequested():
        #             xbmc.log("SERVICE STARTED",level=xbmc.LOGINFO)
        #             break
        #         xbmc_monitor.waitForAbort(1)
        # xbmc_monitor.waitForAbort(1)
        # xbmc.log("REDIRECTING",level=xbmc.LOGINFO)
        # # if not ADDON.getSetting(SETTINGS.RUN_AT_STARTUP):
        # replace_plugin_url(ROUTE.ROOT)
        return True


# end of single file code


socket_client = SocketClient()
tries = 300


def run(argv):
    url, handle, query = argv[0], int(argv[1]), argv[2]
    address = None
    xbmc.log("RUN SCC",level=xbmc.LOGINFO)
    for i in range(tries):
        xbmc.log("TRYING TO CONNECT TO WS", level=xbmc.LOGINFO)
        if stop_if_updating(handle):
            xbmc.log("STOPPING UPDATE",level=xbmc.LOGINFO)
            return
        try:
            address = socket_client.get_address()
            if address:
                response, address = socket_client.send('handshake', address)
                xbmc.log("HANDSHAKED",level=xbmc.LOGINFO)
                if response == b'love you':
                    break
        except socket_error as e:
            xbmc.log("error", level=xbmc.LOGINFO)
            xbmc.log(repr(e), level=xbmc.LOGINFO)
            pass
        sleep(0.1)
    if address:
        xbmc.log("SENDING REQ TO SERVICEE",level=xbmc.LOGINFO)
        socket_client.settimeout(None)
        socket_client.send(dumps({'handle': handle, 'url': url, 'query': query}), address)
    else:
        xbmc.log("END NO ADDRESS",level=xbmc.LOGINFO)
        endOfDirectory(handle, cacheToDisc=False)
        executebuiltin("XBMC.ActivateWindow(Home)")
