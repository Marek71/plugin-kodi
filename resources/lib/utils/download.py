# -*- coding: utf-8 -*-
import os
import time
import xbmcvfs
import re

from resources.lib.const import SETTINGS, ROUTE, DOWNLOAD_STATUS, DOWNLOAD_SERVICE_EVENT, SERVICE, LANG, URL, HTTP_METHOD, MEDIA_TYPE, PROTOCOL
from resources.lib.gui.directory_items import InfoDialog
from resources.lib.kodilogging import logger
from resources.lib.storage.settings import settings
from resources.lib.storage.sqlite import SQLiteStorage, DB
from resources.lib.utils.kodiutils import refresh, current_path, get_string, get_file_path, kodi_json_request, execute_json_rpc, fixed_xbmcvfs_exists
from resources.lib.wrappers.http import Http

from resources.lib.gui.renderers.media_info_renderer import MediaInfoRenderer
from resources.lib.subtitles import Subtitles
from resources.lib.api.api import API
from resources.lib.defaults import Defaults

def microtime():
    return float(time.time() * 1000)


def get_percentage(pos, total_length):
    return int(100 * pos / total_length)


def dir_check(path):
    dirs_to_check = [] 
    if re.search(r'Season [0-9][0-9]$', path): dirs_to_check.append(os.path.dirname(path))
    dirs_to_check.append(path)
    return dirs_to_check


def download_subtitles(filename, media_id): 
    subs = []
    subtitles_string = False
    api = Defaults.api()
    try:
        media, _ = api.api_response_handler(api.request(HTTP_METHOD.GET, API.URL.media_detail(media_id)))
        media_data = MediaInfoRenderer.merge_media_data(media)
        labels = media_data['info_labels']
        if labels.get('mediatype') == MEDIA_TYPE.EPISODE:
                labels['TVShowTitle'] = media['parent_info_labels']['originaltitle'][0]
        subtitles_string = Subtitles.build_search_string(labels)
        if subtitles_string:
            Subtitles.get(media_data, subtitles_string, subs, os.path.splitext(filename)[0])
    except:
        logger.error('Error during getting subtitles for downloaded file.')
    return


def write_nfo(dest, name, media_id):
    api = Defaults.api()
    try:
        media_detail, _ = api.api_response_handler(api.request(HTTP_METHOD.GET, API.URL.media_detail(media_id)))
        
        media_type = 'movie'
        if media_detail.get('info_labels').get('mediatype') == MEDIA_TYPE.TV_SHOW: # Its TVShow, strip Seasons from filename
            media_type = 'tv'
            filename = get_file_path(os.path.dirname(dest), 'tvshow.nfo')
        if media_detail.get('info_labels').get('mediatype') == MEDIA_TYPE.EPISODE: # Its Episode, no need to save episode .nfo
            return
        else: 
            filename = get_file_path(dest, u'{0}.nfo'.format(os.path.splitext(name)[0])) # Its movie
        if 'services' in media_detail:
            services = media_detail['services']
            nfo_file = xbmcvfs.File(filename, 'w')
            if 'csfd' in services:
                csfd_id = services['csfd']
                nfo_file.write(PROTOCOL.HTTPS + ':' + URL.CSFD_TITLE.format(csfd_id) + "\n")
            if 'tmdb' in services:
                tmdb_id = services['tmdb']
                nfo_file.write(PROTOCOL.HTTPS + ':' + URL.TMDB_TITLE.format(media_type, tmdb_id) + "\n")
            if 'imdb' in services:
                imdb_id = services['imdb']
                nfo_file.write(PROTOCOL.HTTPS + ':' + URL.IMDB_TITLE.format(imdb_id) + "\n")
            nfo_file.close()
    
    except:
        logger.error('Error during getting and writing .nfo file.')
    return


def download(url, dest, name, dl_id, media_id, subs_included):
    for path in dir_check(dest): 
        if not fixed_xbmcvfs_exists(path):
            xbmcvfs.mkdir(path)

    db = SQLiteStorage.Download()
    filename = get_file_path(dest, name)
    logger.debug("Downloading %s to %s " % (str(url), filename))
    try:
        if filename[:3] == 'smb' or filename[:3] == 'nfs':
            f = xbmcvfs.File(filename, 'wb')
        else:
            try:
                f = open(filename, 'ab+')
            except:
                f = xbmcvfs.File(filename, 'wb')  # Because non-consistent behauviour of open in justOS/Win, on CERTAIN os for CERTAIN alphabets like Japanese could have open problems, but xbmcvfs doesnt have an issue.
    except:
        logger.error('Error: Cant open file for write.')
        return

    headers = {}
    if os.path.exists(filename):
        pos = os.stat(filename).st_size
    else:
        pos = 0
    if pos:
        headers['Range'] = 'bytes={pos}-'.format(pos=pos)
        logger.debug('Resuming download from position %s' % pos)
    r = Http.get(url, headers=headers, stream=True)
    try:
        total_length = int(r.headers.get('content-length'))
    except:
        logger.error('Error: There is an issue with getting file for download.')
        return
    chunk = min(
        32 * 1024 * 1024,
        (1024 * 1024 *
         4) if total_length is None else int(total_length / 100))

    last_notify = None
    last_time = microtime()
    done = get_percentage(pos, total_length)
    db.dl_update(dl_id, done, 0, total_length)
    if ROUTE.DOWNLOAD_QUEUE == current_path():
        refresh()
    dl = 0

    for data in r.iter_content(chunk):
        notify_percent = settings[SETTINGS.DOWNLOADS_NOTIFY]
        status = DB.DOWNLOAD.get_status(dl_id)
        if status != DOWNLOAD_STATUS.DOWNLOADING:
            logger.debug('Downloading cancelled because status changed to %s' % status)
            if status == DOWNLOAD_STATUS.CANCELLED:
                f.close()
                xbmcvfs.delete(filename)
            return
        if total_length is not None:
            dl += len(data)
            t = microtime()
            if t > last_time:
                kbps = int(
                    float(len(data)) / float((t - last_time) / 1000) / 1024)
                done = get_percentage(dl + pos, total_length + pos)
                last_time = t
                if notify_percent != 0 and last_notify != done and (
                        done % notify_percent) == 0:
                    db.dl_update(dl_id, done, kbps, total_length)
                    InfoDialog("%s%% - %d kB/s" % (done, kbps), name).notify()
                    if ROUTE.DOWNLOAD_QUEUE == current_path():
                        refresh()
                    last_notify = done

        else:
            dl += 0
        f.write(data)
    f.close()
    db.done(dl_id)
    
    if settings[SETTINGS.SUBTITLES_PROVIDER_AUTO_SEARCH] and subs_included != 1:
        download_subtitles(filename, media_id)
        
    write_nfo(dest, name, media_id)
    
    InfoDialog(get_string(LANG.SUCCESSFULLY_DOWNLOADED), name).notify()
    if settings[SETTINGS.DOWNLOADS_FOLDER_LIBRARY] and settings[SETTINGS.LIBRARY_AUTO_UPDATE]:
        execute_json_rpc({"jsonrpc": "2.0", "id": 1, "method": "VideoLibrary.Scan"})    # User using downloads to library path and wants to refresh after adding item 
    kodi_json_request(SERVICE.DOWNLOAD_SERVICE, DOWNLOAD_SERVICE_EVENT.ITEM_ADDED, {})  # because we dont know if something isnt next on queue

    if ROUTE.DOWNLOAD_QUEUE == current_path():
        refresh()
